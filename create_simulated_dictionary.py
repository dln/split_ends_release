'''
create_simulated_dictionary.py 

usage:
python create_simulated_dictionary.py -o lexicon.csv -er X -ec Y -rc Z -n N

Creates a new lexicon in the file lexicon.csv, with format matching the ones
produced by compute_lexical_properties.py.  The new lexicon has N words, each
with three properties, called explanatory, response, and control.  Each field
is 0-centered with standard deviation 1, and
    covariance_matrix = [[1, X, Y],
                         [X, 1, Z],
                         [Y, Z, 1]].

This version:  December 2017.
'''

import numpy
import argparse

def create_simulated_dictionary(expl_resp, expl_cont, resp_cont, n):
    '''
    expl_resp:  correlation between explanatory and response
    expl_cont:  correlation between explanatory and control
    resp_cont:  correlation between response and control
    n:          number of data points to generate.
    '''
    words = ["word_%d" % i for i in range(1,n+1)]
    means = [0, 0, 0]
    covariance_matrix = [[1, expl_resp, expl_cont],
                         [expl_resp, 1, resp_cont],
                         [expl_cont, resp_cont, 1]]
    results = numpy.random.multivariate_normal(means, covariance_matrix, n)
    result = {}
    for i in range(n):
        result[words[i]] = list(results[i])
    return result

def write_simulated_dictionary(dictionary, outfile):
    f = open(outfile, "w")
    f.write("word,explanatory,response,control\n")
    for word in dictionary.keys():
        values = dictionary[word]
        f.write("%s,%0.5f,%0.5f,%0.5f\n" % (word,values[0],values[1],values[2]))
    f.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='create synthetic lexicons')
    parser.add_argument('-n', '--lex_size', type=int, default=1081,
                        help="How many words would you like?")
    parser.add_argument('-er', '--expl_resp', type=float, default=0.0,
                        help="correlation between explanatory and response")
    parser.add_argument('-ec', '--expl_cont', type=float, default=0.0,
                        help="correlation between explanatory and control")
    parser.add_argument('-rc', '--resp_cont', type=float, default=0.0,
                        help="correlation between response and control")
    parser.add_argument('-o', '--output_file', type=str, default="outfile.csv",
                        help="output lexicon file")
    args = parser.parse_args()

    D = create_simulated_dictionary(args.expl_resp, args.expl_cont,
                                    args.resp_cont, args.lex_size)
    write_simulated_dictionary(D, args.output_file)
