# README #

### Current system requirements: ###
* Python 2.7 (or higher version of Python 2), with the following packages:
    * scipy (version >= 1.0) 
    * numpy (version >= 1.11)
    * pandas (version >= 0.21)
    * statsmodels (version >= 0.9)
* Java (including `javac`)
* Gurobi
    * ensure that GUROBI_HOME is set
    * ensure that $GUROBI_HOME/bin is in your PATH
    * ensure that $GUROBI_HOME/lib/gurobi.jar (and `.`, the working directory) is in your CLASSPATH
* LaTeX (specifically `pdflatex`, with the `tikz`, `pgfplots`, `fullpage`, and `xcolor` packages)
* R (specifically `Rscript` with the `lme4` package)
* Make

### To test your setup ###
* Install and configure the tools listed above (make, Python, Java, LaTeX, Gurobi, R).
* Try `make test`.  You should see generally affirming comments if everything is working properly, and error messages if not.

### To run the experiments in the paper ###
* Type `make full`.

### Data files and their origin ###
* `data/ELP_40k_lexicon.csv`:  lexicon data from the English Lexicon Project [Balota et al, 2007].
    * Homophones were combined; schwas and wedges were combined into wedges.
    * Frequency counts come from the SUBTLEX counts, [Brysbaert and New, 2009].
        * `raw_freq_count` is the raw count from SUBTLEX
        * `LgSUBTLWF` is the log base 10 of raw count

* `data/SWR1081.csv`:  spoken-word recognition data collected by Mitchell Sommers and John Morton.  Descriptions of the fields:
    * `word`:  word written as it is found in ELP.
    * `Original Orthography`:  word written as it was found in [Sommers and Morton, 2016].
    * `Pron`:  pronunciation of the word (in either `word` or `Original Orthography` form).
    * `ACC`:  data from [Sommers and Morton, 2016].
    * `Freq`: data from [Brysbaert and New, 2009].
    * `OrthLength`:  number of letters in the orthography of the word as written in `Original Orthography` (there are 18 words where the word length between `word` and `Original Orthography` differs by ±1).
    * `familiarity`:   data from the Hoosier Mental Lexicon [Nusbaum, Pisoni and Davis, 1984].
    * `phonprob`:  data from Michael Vitevitch's online calculator [Vitevitch and Luce, 2004].
    * `duration`:  data extracted from the audio files used in [Sommers and Morton, 2016].

* `data/SWR1081_raw_data.csv`:  the raw data associated with `data/SWR1081.csv`.  
    * The fields:
         * `List`:  ???
         * `PID`:  Participant identifier
         * `CRESP`:  the correct response
         * `RESP`:  the response given
         * `ACC`:  0 (RESP was coded as incorrect) or 1 (RESP was coded as correct) in our data.
         * `DEPRECATED_ACC`:  0 (RESP was coded as incorrect) or 1 (RESP was coded as correct) by Sommers & Morton:
             * we took raw data from Sommers & Morton.
             * we transcribed CRESP and RESP to phonological forms
             * we counted as correct when phonological forms match (e.g., days and daze)
             * we also corrected errors such that RESP does not form a real word, and either
                 * A) it forms a common misspelling (“thief” for “theif”), or 
                 * B) it is spelled in a phonetically possible way (e.g., “burch” for “birch”). 
             * these resulted in changing 240 responses (1.2% of incorrect answers)

* `data/SWR1081-extended.csv`:  additional fields are calculated based on the above data:
    * `ELPdegree`:  number of neighbors in ELP (as described above).
    * `ELPCC`:  clustering coefficient in ELP.
    * `ELPC1_nbrs`:  number of neighbors in ELP derived from a substitution in the first consonant (i.e., for a word ABC [A and C are consonants, B is a vowel], the number of words DBC in ELP).
    * `ELPV_nbrs`:  number of neighbors in ELP derived from a substitution in the vowel (i.e., for a word ABC [A and C are consonants, B is a vowel], the number of words AEC in ELP).
    * `ELPC2_nbrs`:  number of neighbors in ELP derived from a substitution in the second consonant (i.e., for a word ABC [A and C are consonants, B is a vowel], the number of words ABF in ELP).
    * `ELPspread`:  number of positions in which the word has a substitution neighbor in ELP (i.e., the number of nonzero values in {`ELPC1_nbrs`, `ELPV_nbrs`, `ELPC2_nbrs`}).
    * `ELPavg_nbr_log_freq`:  sum the log frequency (from SUBTLEX, as described above) over all of the word's neighbors, treating `NULL` as `0`, and divide by the number of neighbors.
    * `ELPavg_nbr_raw_freq`:  sum the raw frequency (from SUBTLEX, as described above) over all of the word's neighbors, treating `NULL` as `0`, and divide by the number of neighbors.
    


