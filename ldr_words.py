# Source file encoding: utf-8

'''
ldr_words.py

An implementation of Hidden Markov Models for the paper "Making
Long-Distance Relationships Work: Quantifying Lexical Competition with
Hidden Markov Models", by Julia Strand and David Liben-Nowell.

This file:  classes/code related to single words and lists of words.

This version:  October 2015.
'''

import sys
import math
TOLERANCE = 0.0000001

# Each of the following entries maps Excel-Friendly IPA -> IPA.
IPA_CONSONANT_MAP =  {'b' : 'b',  '©' : 't∫', 'd' : 'd',  'f' : 'f',  'g' : 'g',
                      'h' : 'h',  'ĵ' : 'dʒ', 'k' : 'k',  'l' : 'l',  'm' : 'm',
                      'n' : 'n',  'ŋ' : 'ŋ',  'p' : 'p',  'r' : 'ɹ',  's' : 's',
                      '∫' : '∫',  't' : 't',  'θ' : 'θ',  'ð' : 'ð',  'v' : 'v',
                      'w' : 'w',  'y' : 'j',  'z' : 'z',  'ʒ' : 'ʒ',
                      ## the following appear only in HML, not in ELP.
                      'L' : 'ḷ',  'N' : 'ṇ',  'M' : 'm̩'}

IPA_VOWEL_MAP =      {'!' : 'i',  'I' : 'ɪ',  'ɛ' : 'ɛ',  'e' : 'e',  'æ' : 'æ',
                      'ɑ' : 'ɑ',  'Ă' : 'aʊ', 'Ĩ' : 'aɪ', 'ʌ' : 'ʌ',  'ɔ' : 'ɔ',
                      'œ' : 'oɪ', 'o' : 'o',  'ʊ' : 'ʊ',  'u' : 'u',  'ɝ' : 'ɝ',
                      'ə' : 'ə',  'ø' : 'ɨ',
                      ## the following appear only in HML, not in ELP.
                      'ɚ' : 'ɚ'}

CONSONANTS = [ch.decode("utf-8") for ch in IPA_CONSONANT_MAP.keys()]
VOWELS =     [ch.decode("utf-8") for ch in IPA_VOWEL_MAP.keys()]
ALPHABET =   CONSONANTS + VOWELS


class Word:
    def __init__(self, 
                 orthography = None, 
                 pronunciation = None,
                 frequency_count_string = None,  
                 log_frequency_string = None,
                 num_syllables = None,
                 lexicon_origin = None):   # !!!!
        '''
        A Word class, with support for both a Hidden Markov Model of
        this word and Levenshtein/Hamming distance calculations to
        other words.

        Parameters (see any row of https://apps.carleton.edu/curricular/psyc/
           jstrand/assets/LDR_sample_lexicon.csv for an example of these data):
          orthography:           the English spelling of the word.
          pronunciation:         the pronunciation, in Excel-Friendly IPA
          frequency_count:       raw frequency count ("NULL" if count == 0)
          log_frequency_string:  log_10 of frequency + 1 ("NULL" if count == 0)
          num_syllables:         the number of syllables in the word.
        '''
        self.word = orthography
        self.ipa = pronunciation
        self.lexicon_origin = lexicon_origin
        if frequency_count_string == log_frequency_string == "NULL":
            self.raw_frequency = None
            self.subtitle_log_frequency = None
        else:
            self.raw_frequency = int(frequency_count_string)
            self.subtitle_log_frequency = float(log_frequency_string)
        self.syllables = num_syllables
        self.cv_pattern = "".join(['C'*(ch in CONSONANTS) + 'V'*(ch in VOWELS) 
                                   for ch in self.get_pronunciation()])
        for ch in [ch for ch in self.get_pronunciation() if ch not in CONSONANTS+VOWELS]:
            print "MISSING!", ch
        assert len(self.cv_pattern) == len(self.ipa)
        self.hmm = None
        self.compressed_hmm = None

    def get_orthography(self):     return self.word
    def get_pronunciation(self):   return self.ipa
    def get_raw_frequency(self):   return self.raw_frequency
    def get_log_frequency(self):   return self.subtitle_log_frequency
    def get_syllable_count(self):  return self.syllables
    def is_monosyllabic(self):     return self.syllables == 1
    def is_univocalic(self):       return self.cv_pattern.count("V") == 1
    def get_cv_pattern(self):      return self.cv_pattern
    def is_CVC(self):              return self.cv_pattern == "CVC"
    def get_lexicon_origin(self):  return self.lexicon_origin

    def hamming_distance(self, other):
        '''
        Computes the Hamming distance (number of differing positions)
        between two words' pronunciations.  If the pronuncations have
        different lengths, returns sys.maxint.
        '''
        self_pronunciation = self.get_pronunciation()
        other_pronunciation = other.get_pronunciation()
        if len(self_pronunciation) != len(other_pronunciation):
            return sys.maxint
        else:
            count = 0
            for i in range(len(self_pronunciation)):
                count += (self_pronunciation[i] != other_pronunciation[i])
            return count


    def edit_distance(self, other):
        '''
        Computes the Levenshtein (edit) distance between two words'
        pronunciations (where ED(x,y) = the smallest number of
        insertions/deletions/substititions required to transform x into y).
        '''
        self_pronunciation = self.get_pronunciation()
        other_pronunciation = other.get_pronunciation()

        opt = {}             # Goal:  opt[i,j] = ED(self_pronunciation[0..i],
                             #                      other_pronunciation[0...j]).
        opt[-1,-1] = 0      
        for i in range(len(self_pronunciation)):
            opt[i,-1] = opt[i-1,-1] + 1
        for j in range(len(other_pronunciation)):
            opt[-1,j] = opt[-1,j-1] + 1

        for i in range(len(self_pronunciation)):
            best_for_i = sys.maxint # at j loop end, best_for_i = min_j opt[i,j]
            for j in range(len(other_pronunciation)):
                sub_cost_ij = (self_pronunciation[i] != other_pronunciation[j])
                opt[i,j] = min(opt[i-1,j-1] + sub_cost_ij, # substitute self[i]
                                                           #    for other[j]
                               opt[i-1,j]   + 1,           # delete self[i]
                               opt[i,j-1]   + 1)           # insert other[j]
                best_for_i = min(best_for_i, opt[i,j])

        return opt[len(self_pronunciation) - 1, len(other_pronunciation) - 1]


    def get_hmm(self, confusion_probabilities):
        '''
        Returns the WordHMM for this word, as described in build_WordHMM(),
        constructing it (and caching it) if necessary.
        '''
        if self.hmm == None:
            self.build_WordHMM(confusion_probabilities)
        return self.hmm


    def get_compressed_hmm(self, confusion_probabilities):
        '''
        Returns the compressed WordHMM for this word (with all
        NULL-outputting states short-circuited), as described in
        ldr_hmm.py, constructing it (and caching it) if necessary.
        '''
        if self.compressed_hmm == None:
            uncompressed_hmm = self.get_hmm(confusion_probabilities)
            self.compressed_hmm = ldr_hmm.Compressed_HMM(uncompressed_hmm)
        return self.compressed_hmm


    def build_WordHMM(self, confusion_reporter):
        '''
        Constructs a custom-built HMM for this word.  An n-phoneme
        word generates an (2n+3)-state HMM {-1A[start], OA,...,nA[final] 
        + -1B, 0B,...,(n-1)B} with the structure

          START                                                        FINAL
           -1A ---------> 0A ----------> 1A  ....   (n-1)A -----------> nA
             \_          _/  \_         _/              \_             _/
               \-> -1B -/      \-> 0B -/                  \-> (n-1)B -/  

        where there are also self-loops on all the B states.  

        The emission probabilities are given as follows:
          -- in a state iA, for each phoneme X (including W_i and NULL), we 
             emit X with probability Pr[hear phoneme X|presented with W_i].
          -- in a state iB, for each non-NULL phoneme X we emit X with 
             probability Pr[hear phoneme X|presented with NULL].
        (START and FINAL emit nothing.)

        The transition probabilities are given by the total hallucination 
        probability P (see ldr_confusion.py for total_hallucinate_prob()):
        for a state iA or iB, we have 
          -- Pr[(i+1)A] = 1 - P
          -- Pr[iB]     = P.
        (Except for FINAL = nA, which just loops.)

        The relevant phoneme confusion data is passed in as
        confusion_reporter (an instance of ConfusionProbs); see the
        documentation in ldr_confusion.py for details.
        '''

        astate = lambda i:str(i)+"A"
        bstate = lambda i:str(i)+"B"
        N = len(self.get_pronunciation())

        # States of the HMM:
        states = [astate(i) for i in range(-1,N+1)] \
                   + [bstate(i) for i in range(-1,N)]
        start = astate(-1)
        final = astate(N)

        # Transition probabilities:
        trans_probs = {astate(N) : {astate(N) : 1.0}}
        for i in range(-1,N):
            trans_probs[astate(i)] = {
                astate(i+1) : 1 - confusion_reporter.total_hallucinate_prob(),
                bstate(i): confusion_reporter.total_hallucinate_prob()
                }
            trans_probs[bstate(i)] = trans_probs[astate(i)]

        # Emission probabilities:
        emit_probs = {astate(-1) : {None : 1.0}, astate(N) : {None : 1.0}}
        emit_probs[bstate(-1)] = confusion_reporter.hallucinate_dictionary()
        for i in range(0,N):
            emit_probs[bstate(i)] = confusion_reporter.hallucinate_dictionary()
            phoneme = self.get_pronunciation()[i]
            emit_probs[astate(i)] = confusion_reporter.emit_dictionary(phoneme)

        self.hmm = ldr_hmm.HMM(states, start, final, emit_probs, trans_probs)


        
class Lexicon:
    def __init__(self, dictionary_filename, freq_thresh=None):
        '''
        A Lexicon class storing a dictionary of Word objects, built
        from each row of a CSV dictionary file (which must have as a
        header row precisely "_Word,Pron,raw_freq_count,LgSUBTLWF,NSyll")
        !!!
        '''
        self.dictionary = {}
        self.C_raw_count = 0
        self.V_raw_count = 0
        f = open(dictionary_filename)
        header_row = f.readline().decode("utf-8").strip()
        isELP = (header_row == "_Word,Pron,raw_freq_count,LgSUBTLWF,NSyll")
        isHML = (header_row == "Word,Pron,KF freq,SubtitleFreq")
        assert (isELP or isHML)
        for line in f:
            fields = map(lambda x: x.decode("utf-8"), line.strip().split(","))
            if isHML:
                # using "KF freq" field for now, with no raw counts or syllable
                # counts.  Also 14 words have bogus KF freq values.  !!!
                if fields[2] == "#VALUE!":
                    fields[2] = "0"
                if fields[2] == "":
                    fields[2] = "NULL"
                fields = [fields[0], fields[1], 0, fields[2], 0]
                if fields[3] == "NULL":
                    fields[2] = "NULL"
            word = Word(orthography=fields[0],
                        pronunciation=fields[1],
                        frequency_count_string=fields[2],
                        log_frequency_string=fields[3],
                        num_syllables=int(fields[4]))
            if not freq_thresh or (word.get_log_frequency() and \
                                       word.get_log_frequency() > freq_thresh):
                self.dictionary[word.get_orthography()] = word
            word_freq = word.get_raw_frequency()
            if word.get_raw_frequency():
                self.C_raw_count += word.get_cv_pattern().count("C") * word_freq
                self.V_raw_count += word.get_cv_pattern().count("V") * word_freq
        f.close()

    def get_words(self):           return self.dictionary.values()
    def get_word(self, word):      return self.dictionary[word]
    def has_word(self, word):      return word in self.dictionary.keys()
    def get_consonant_count(self): return self.C_raw_count
    def get_vowel_count(self):     return self.V_raw_count


if __name__ == "__main__":
    dictionary_file = "../data/40kLexicon_nohomoph_noschwa.csv"
    print "Loading words from %s" % dictionary_file
    lexicon = Lexicon(dictionary_file)
    print "Successfully loaded %d words." % len(lexicon.get_words())
    
    # Ensure that we have no homophones in the lexicon:
    pronunciations = {}
    for word in lexicon.get_words():
        assert word.get_orthography() not in pronunciations
        pronunciations[word.get_orthography()] = True
    print "Great news!  No homophones found."

    # Ensure that log frequencies and raw frequencies match;
    # our log frequency is supposed to be log_10 of (the raw frequency + 1)
    # (i.e., the dataset uses plus-one smoothing in its log frequencies).
    for word in lexicon.get_words():
        if word.get_log_frequency():
            calculated_log_freq = math.log(word.get_raw_frequency() + 1, 10)
            stored_log_freq = word.get_log_frequency()
            assert math.fabs(calculated_log_freq - stored_log_freq) < TOLERANCE
    print "Great news!  Our raw- and log-frequency numbers match."

    print "When I weight by frequency, I see %d consonants and %d vowels." \
        % (lexicon.get_consonant_count(), lexicon.get_vowel_count())
        


    

