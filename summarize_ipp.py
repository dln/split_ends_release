'''
summarize_ipp.py

usage:
python summarize_ipp.py -l lex.csv -d dir -r response -x explanatory -c controls

Generates  outputs 7 files summarizing the ILP output:
 -- scatter.csv (for each run in the ILP:  response mean diff, variance sum)
 -- words.csv (for each word in lex.csv:  word,expl,resp,blue,red)
 -- ipp_significance.tex (significance lines for a plot, based on set sizes.
 -- results_{lo,hi}-words.txt (list of words chosen in each trial)
 -- results_{lo,hi}-expl+resp+controls.txt (average expl/resp/control values 
        for each trial.

Looks for ILP output in dir/ippOut.csv and dir/ippOut_x.csv.

This version:  June 2018.
'''

import pandas
import sys
import argparse
import scipy.stats


def load_output_csv(filename, weighted=True):
    '''
    Load the CSV files produced by the Java ILP (see ippClean.java),
    into a list of pairs (each pair is [A,B], with A and B sets of words.
    '''
    f = open(filename)

    # skip over #-commented lines and the dimensions line (line #1).
    line = f.readline()
    while (not line or line[0] == "#"):  
        line = f.readline()

    # extract the list of words, comma separated, A followed by B (line #2)
    words = f.readline().strip().split(",")
    words_A, words_B = words[0:int(len(words)/2)], words[int(len(words)/2):]
    n = len(words_A)

    # Process lines 3, 4, ...:  A bitvector, A rand wts, B bitvector, B rand wts
    sets = []
    for line in f:
        fields = line.strip().split(",")
        A_vector = list(map(int, fields[0:n]))
        if weighted:
            B_vector = list(map(int, fields[2*n:3*n]))
        else:
            B_vector = list(map(int, fields[n:2*n]))
        sets.append(([words_A[i] for i in range(n) if A_vector[i]],
                     [words_B[i] for i in range(n) if B_vector[i]]))
    f.close()
    return sets

def output_ipp_csv(outputdir, controls, resp, words_df, randoms):
    C = len(controls)
    f_out = open(outputdir + "/scatter.csv", "w")
    f_out.write("%s,%s" % ("resp_mean_diff" ,"resp_variance_sum"))
    for i in range(C):
        f_out.write(",C%d_mean,C%d_stddev" % (i,i))
    f_out.write("\n")

    for trial in randoms:
        A = words_df[words_df['word'].isin(trial[0])]
        B = words_df[words_df['word'].isin(trial[1])]
        AB = words_df[words_df['word'].isin(trial[0] + trial[1])]
        resp_mean_diff = B[resp].mean() - A[resp].mean()
        resp_variance_sum = (A[resp].std(ddof=0))**2 + (B[resp].std(ddof=0))**2
        f_out.write("%f,%f" % (resp_mean_diff, resp_variance_sum))
        for i in range(C):
            f_out.write(",%f,%f" % (AB[controls[i]].mean(), 
                                    AB[controls[i]].std(ddof=0)))
        f_out.write("\n")
    f_out.close()


def output_words_csv(outputdir, expl, resp, words_df, blueSet, redSet):
    f_out = open(outputdir + "/words.csv", "w")
    f_out.write("%s,%s,%s,%s,%s\n" 
                % ("word", "expl", "resp", "blue", "red"))
    for index, row in words_df.iterrows():
        try:
            float(row[expl]), float(row[resp])
            f_out.write("%s,%0.4f,%0.4f,%d,%d\n" %
                        (str(row['word']).lower(),  # dies on 'nan' without str
                         row[expl],
                         row[resp],
                         row['word'] in blueSet[0] + blueSet[1],
                         row['word'] in redSet[0] + redSet[1]))
        except ValueError:
            continue
    f_out.close()

def summarize_human_readable(outputdir, expl, resp, control, words_df, trials):
    def write_file(filename, word_function, evaluator):
        f_out = open(outputdir + "/" + filename, "w")
        for i in range(len(trials)):
            f_out.write("%d\t%s\n" % (i+1, evaluator(word_function(i))))
        f_out.close()

    lo = lambda i: words_df[words_df['word'].isin(trials[i][0])]
    hi = lambda i: words_df[words_df['word'].isin(trials[i][1])]
    get_word_text = lambda S: " ".join(list(S['word'].values))
    get_variables = lambda S: " ".join(["%1.3f" % S[v].mean()
                                        for v in [expl, resp] + control])

    write_file("results_lo-words.txt", lo, get_word_text)
    write_file("results_hi-words.txt", hi, get_word_text)
    write_file("results_lo-expl+resp+controls.txt", lo, get_variables)
    write_file("results_hi-expl+resp+controls.txt", hi, get_variables)
        

def main():
    parser = argparse.ArgumentParser(description="summarize IPP output")
    parser.add_argument('-l', '--lexicon', type=str, help = "lexicon.csv")
    parser.add_argument('-d', '--outputdir', type=str, 
                        help = "output directory.  I assume ippOut.csv " +
                        "and ippOut_x.csv are present!")
    parser.add_argument('-x', '--expl', type=str, help = "explanatory variable")
    parser.add_argument('-r', '--resp', type=str, help = "response variable")
    parser.add_argument("-c", "--controls", dest='controls', nargs = "*",
                    default=[], help='list of control variables')
    args = parser.parse_args()

    if None in vars(args).values():
        raise TypeError("required arguments not specified")

    words_df = pandas.read_csv(args.lexicon)
    randoms = load_output_csv(args.outputdir + "/ippOut.csv")
    extremes = load_output_csv(args.outputdir + "/ippOut_x.csv", weighted=False)

    output_ipp_csv(args.outputdir, args.controls, args.resp, words_df,
                       randoms)
    output_words_csv(args.outputdir, args.expl, args.resp, words_df,
                     extremes[1], extremes[0])
    summarize_human_readable(args.outputdir, args.expl, args.resp,
                             args.controls, words_df, randoms)
                             

    # -- ipp-significance.tex -----
    k = len(extremes[0][0])
    f_out = open(args.outputdir + "/ipp-significance.tex", "w")
    for (p,name) in [(0.01,'A'), (0.05,'B'), (0.10,'C')]:
        t_star = scipy.stats.t.ppf(1-p/2, 2*k - 2)
        f_out.write("\\addplot+[no marks,smooth,domain=-0.25:0.25,"
                    + "name path=%s,black] {%d*x^2 / (%0.4f^2)};\n"
                    % (name, k, t_star))
    f_out.close()

if __name__ == "__main__":
    main()
