/*
ipp.java

usage:
java ipp low.csv high.csv out.csv K delta T

where low.csv and high.csv are the two populations (like Lz.csv and
Hz.csv as output by splits.py), out.csv is the file into which runs
are output, K is the set size (number of low/high elements to choose),
delta is the tolerance in each control dimension, and T is the number
of trials to run.

This version:  December 2017.
*/

import java.util.*;
import java.io.*;
import gurobi.*;

public class ipp {

  // Max time gurobi spends per trial (in seconds)
  public static final double GUROBI_TIME_LIMIT = 60;

  // Whether gurobi prints progress to terminal
  public static final boolean GUROBI_VERBOSE = false;

  public static final boolean RECORD_WEIGHTS = true;
  public static final boolean PRINT_TRIAL_PROGRESS = true;
  public static final boolean PRINT_TRIAL_SUMMARY = false;
  public static final boolean GENERATE_XTREME = true;
  public static final double INT_TOL = 0.00001;

  // Load parameters from command line input:
  //   input files, output files, k, delta, trials.
  // Returns whether the appropriate number of parameters were passed in.
  // Note: this doesn't set n, d, or create FileWriters.
  public static boolean loadParams(String[] args, Params p) {
    if(args.length < 6) {
      System.out.println("WARNING: 6 args expected:  "
                         + "inputA.csv, inputB.csv, output.csv, "
                         + "set size, tolerance, no. of trials.");
      System.out.println("E.g.: java ipp Lz.csv Hz.csv out.csv 50 0.05 100");
      return false;
    }
    else {
      p.aName = args[0];
      p.bName = args[1];
      p.outName = args[2];
      p.k = Integer.parseInt(args[3]);
      p.delta = Double.parseDouble(args[4]);
      p.trials = Integer.parseInt(args[5]);
      return true;
    }
  }

  // Loads the word set given a file name to a preconstructed, empty
  // array list of words.
  public static void loadWordSet( String fname, ArrayList<Word> words) {
    try {
      Scanner sc = new Scanner( new File( fname ) );
      String line = "";

      // loop through lines that start with a #
      while(  sc.hasNextLine() ) {
        line = sc.nextLine();
        if( line.length() == 0 || line.charAt(0) != '#' ) {
          break; // yer done. get on with the rest of the file
        }
      }

      // Current "line" is the fields
      System.out.println("Fields in "+fname+":\n   "+ line );

      // read in and ignore fields
      line = sc.nextLine();

      // this next line should tell me n and d, comma separated
      String[] tokens = line.split(","); // n, d
      int n = Integer.parseInt(tokens[0]); // n
      int d = Integer.parseInt(tokens[1]); // d

      // now read in the rest of the file and fill in M
      while( sc.hasNextLine() ){
        tokens = sc.nextLine().split(",");
        String word = tokens[0];
        double resp = Double.parseDouble(tokens[1]);
        double exp = Double.parseDouble(tokens[2]);
        double[] controls = new double[d];
        for( int j=0; j < d; j++ ) {
          controls[j] = Double.parseDouble( tokens[j+3] );
        }
        words.add(new Word(word,exp,resp,controls));
      }
    }
    catch( FileNotFoundException e ) {
      e.printStackTrace();
    }
  }

  // Reports whether the word sets have the same size and whether all
  // words have the same number of fields.
  public static boolean verifyWordSets(ArrayList<Word> aSet,
                                       ArrayList<Word> bSet,
                                       Params p) {
    if (aSet.size() != bSet.size()) {
      System.out.println("WARNING: A set and B set have different sizes");
      return false;
    }

    for(Word w : aSet) {
      if (w.controls.length != p.d) {
        System.out.println("WARNING: " + w.word +
                           " has different number of control fields");
        return false;
      }
    }
    for(Word w : bSet) {
      if (w.controls.length != p.d) {
        System.out.println("WARNING: " + w.word +
                           " has different number of control fields");
        return false;
      }
    }
    return true;
  }

  // Creates the header for output (comments, parameters, and word lists).
  public static void writeHeader(FileWriter fw,
                                 ArrayList<Word> aSet,
                                 ArrayList<Word> bSet,
                                 Params p,
                                 boolean writeWeights) throws IOException  {
    fw.write("# First line: A file, B file, word set sizes (n), "
             + "delta, word subset sizes (k), trials\n");
    fw.write("# Next line: words in A, words in B\n");
    if(writeWeights) {
      fw.write("# Remaining lines are length 4n trial vectors: "
               + "subset of A, wts for A, subset of B, wts for B\n");
    }
    else {
      fw.write("# Remaining lines are length 2n trial vectors: " +
               "subset of A, subset of B\n");
    }
    fw.write(p.aName+","+p.bName+","+p.n+","+p.delta+","+p.k+","+p.trials+"\n");
    for( int i=0; i < p.n; i++ ) {
      fw.write(aSet.get(i).word+ ",");
    }
    for( int i=0; i < p.n-1; i++ ) {
      fw.write(bSet.get(i).word+ ",");
    }
    fw.write(bSet.get(p.n-1).word+"\n");
  }

  // Assigns random weights in [0,1] to all words in the list.
  public static void giveRandomWeights(ArrayList<Word> words) {
    Random rand = new Random();
    for(Word w : words) {
      w.weight = rand.nextDouble();
    }
  }

  // Create Gurobi objects, variables, and add common constrains to the models.
  // Namely two sets of size k, balanced within delta across controls
  public static void initIP(GRBObj grbo,
                            ArrayList<Word> aSet,
                            ArrayList<Word> bSet,
                            Params p) {
    try {
      grbo.env = new GRBEnv();
      grbo.env.set(GRB.DoubleParam.TimeLimit, GUROBI_TIME_LIMIT);
      if (!GUROBI_VERBOSE) grbo.env.set(GRB.IntParam.OutputFlag, 0);
      grbo.model = new GRBModel(grbo.env);
      grbo.aVars = new GRBVar[p.n];
      grbo.bVars = new GRBVar[p.n];
      for(int i = 0; i < p.n; i++) {
        grbo.aVars[i] = grbo.model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+i);
        grbo.bVars[i] = grbo.model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "b_"+i);
      }
      // force model to recognize new variables
      grbo.model.update();

      // Adding constraints

      // sum of a vars is k
      GRBLinExpr expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(1.0, grbo.aVars[i]);
      }
      grbo.model.addConstr(expr, GRB.EQUAL, p.k, "cAk");

      // sum of b vars is k
      expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(1.0, grbo.bVars[i]);
      }
      grbo.model.addConstr(expr, GRB.EQUAL, p.k, "cBk");

      // average of each control dimension is within delta
      for(int j = 0; j < p.d; j++) {
        expr = new GRBLinExpr();
        for(int i = 0; i < p.n; i++) {
          expr.addTerm(aSet.get(i).controls[j], grbo.aVars[i]);
          expr.addTerm(-bSet.get(i).controls[j], grbo.bVars[i]);
        }
        grbo.model.addConstr(expr, GRB.LESS_EQUAL, p.delta*p.k, "cPos"+j);
        expr = new GRBLinExpr();
        for(int i = 0; i < p.n; i++) {
          expr.addTerm(-aSet.get(i).controls[j], grbo.aVars[i]);
          expr.addTerm(bSet.get(i).controls[j], grbo.bVars[i]);
        }
        grbo.model.addConstr(expr, GRB.LESS_EQUAL, p.delta*p.k, "cNeg"+j);
      }
    }
    catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
      e.getMessage());
    }
  }

  // Create Gurobi objects, variables, and add common constrains to the models.
  // Namely two sets of size k, balanced within delta across controls
  public static void initIPkev(GRBObj grbo,
                               ArrayList<Word> aSet,
                               ArrayList<Word> bSet,
                               Params p) {
    try {
      grbo.env = new GRBEnv();
      grbo.env.set(GRB.DoubleParam.TimeLimit, GUROBI_TIME_LIMIT);
      if (!GUROBI_VERBOSE) grbo.env.set(GRB.IntParam.OutputFlag, 0);
      grbo.model = new GRBModel(grbo.env);
      grbo.aVars = new GRBVar[p.n];
      grbo.bVars = new GRBVar[p.n];
      for(int i = 0; i < p.n; i++) {
        grbo.aVars[i] = grbo.model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "a_"+i);
        grbo.bVars[i] = grbo.model.addVar(0.0, 1.0, 0.0, GRB.BINARY, "b_"+i);
      }
      // force model to recognize new variables
      grbo.model.update();

      // Adding constraints

      // sum of a vars is k
      GRBLinExpr expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(1.0, grbo.aVars[i]);
      }
      grbo.model.addConstr(expr, GRB.EQUAL, p.k, "cAk");

      // sum of b vars is k
      expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(1.0, grbo.bVars[i]);
      }
      grbo.model.addConstr(expr, GRB.EQUAL, p.k, "cBk");

      // average of each control dimension is within delta
      for(int j = 0; j < p.d; j++) {
        expr = new GRBLinExpr();
        for(int i = 0; i < p.n; i++) {
          expr.addTerm(aSet.get(i).exp*aSet.get(i).controls[j], grbo.aVars[i]);
          expr.addTerm(bSet.get(i).exp*bSet.get(i).controls[j], grbo.bVars[i]);
        }
        grbo.model.addConstr(expr, GRB.LESS_EQUAL, p.delta*p.k, "cPos"+j);
        expr = new GRBLinExpr();
        for(int i = 0; i < p.n; i++) {
          expr.addTerm(aSet.get(i).exp*aSet.get(i).controls[j], grbo.aVars[i]);
          expr.addTerm(bSet.get(i).exp*bSet.get(i).controls[j], grbo.bVars[i]);
        }
        grbo.model.addConstr(expr, GRB.LESS_EQUAL, p.delta*p.k, "cNeg"+j);
      }
    }
    catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
      e.getMessage());
    }
  }

  // Generates a random weighting for the words in A and B, and then
  // makes a call to gurobi to find the min weight pair of subsets A',
  // B' of size k such that the discrepancy between the average value
  // of each field is at most delta.  These sets are written to a line
  // of the output file with 0/1 indicator values.
  public static void getRandomSol(GRBObj grbo,
                                  ArrayList<Word> aSet,
                                  ArrayList<Word> bSet,
                                  Params p) throws IOException {
    try {
      giveRandomWeights(aSet);
      giveRandomWeights(bSet);
      GRBLinExpr expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(aSet.get(i).weight, grbo.aVars[i]);
        expr.addTerm(bSet.get(i).weight, grbo.bVars[i]);
      }
      grbo.model.setObjective(expr, GRB.MINIMIZE);
      grbo.model.optimize();
      for(int i = 0; i < p.n; i++) {
        aSet.get(i).selected =
           (grbo.aVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
        bSet.get(i).selected =
           (grbo.bVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
      }
    }
    catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
      e.getMessage());
    }
  }

  public static void getXtremeSol(GRBObj grbo,
                                  ArrayList<Word> aSet,
                                  ArrayList<Word> bSet,
                                  Params p,
                                  boolean maximize) throws IOException {
    try {
      GRBLinExpr expr = new GRBLinExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(aSet.get(i).resp, grbo.aVars[i]);
        expr.addTerm(-bSet.get(i).resp, grbo.bVars[i]);
      }
      if (maximize) grbo.model.setObjective(expr, GRB.MAXIMIZE);
      else grbo.model.setObjective(expr, GRB.MINIMIZE);
      grbo.model.optimize();

      for(int i = 0; i < p.n; i++) {
        aSet.get(i).selected =
           (grbo.aVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
        bSet.get(i).selected =
           (grbo.bVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
      }
    }
    catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
      e.getMessage());
    }
  }

  public static void getGoodXtremeSol(GRBObj grbo,
                                      ArrayList<Word> aSet,
                                      ArrayList<Word> bSet,
                                      Params p,
                                      boolean maximize,
                                      double alpha) throws IOException {
    try {
      GRBQuadExpr expr = new GRBQuadExpr();
      for(int i = 0; i < p.n; i++) {
        expr.addTerm(aSet.get(i).resp, grbo.aVars[i]);
        expr.addTerm(-bSet.get(i).resp, grbo.bVars[i]);
      }
      for(int i = 0; i < p.n; i++) {
        for(int j = 0; j < p.n; j++) {
          expr.addTerm(alpha*aSet.get(i).resp*aSet.get(j).resp,
                       grbo.aVars[i],
                       grbo.aVars[j]);
          expr.addTerm(alpha*bSet.get(i).resp*bSet.get(j).resp,
                       grbo.bVars[i],
                       grbo.bVars[j]);
        }
      }
      if (maximize) grbo.model.setObjective(expr, GRB.MAXIMIZE);
      else grbo.model.setObjective(expr, GRB.MINIMIZE);
      grbo.model.optimize();

      for(int i = 0; i < p.n; i++) {
        aSet.get(i).selected =
           (grbo.aVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
        bSet.get(i).selected =
           (grbo.bVars[i].get(GRB.DoubleAttr.X) >= 1 - INT_TOL);
      }
    }
    catch (GRBException e) {
      System.out.println("Error code: " + e.getErrorCode() + ". " +
      e.getMessage());
    }
  }

  public static void writeSol(FileWriter fw,
                              ArrayList<Word> aSet,
                              ArrayList<Word> bSet,
                              Params p,
                              boolean writeWeights) throws IOException {
    if(writeWeights) {
      for(int i = 0; i < p.n; i++) {
        fw.write((aSet.get(i).selected?"1":"0")+",");
      }
      for(int i = 0; i < p.n; i++) {
        fw.write(aSet.get(i).weight+",");
      }
      for(int i = 0; i < p.n; i++) {
        fw.write((bSet.get(i).selected?"1":"0")+",");
      }
      for(int i = 0; i < p.n-1; i++) {
        fw.write(bSet.get(i).weight+",");
      }
      fw.write(bSet.get(p.n-1).weight+"\n");
    }
    else {
      for(int i = 0; i < p.n; i++) {
        fw.write((aSet.get(i).selected?"1":"0")+",");
      }
      for(int i = 0; i < p.n-1; i++) {
        fw.write((bSet.get(i).selected?"1":"0")+",");
      }
      fw.write((bSet.get(p.n-1).selected?"1":"0")+"\n");
    }
  }

  public static void printSummary(ArrayList<Word> aSet,
                                  ArrayList<Word> bSet,
                                  Params p) {
    double expA = 0.0;
    double expB = 0.0;
    double expDelta = 0.0;
    double respA = 0.0;
    double respB = 0.0;
    double respDelta = 0.0;
    double respAs2 = 0.0;
    double respBs2 = 0.0;
    double[] controlsA = new double[p.d];
    double[] controlsB = new double[p.d];
    double[] controlsDelta = new double[p.d];
    int aCount = 0;
    int bCount = 0;
    for(int i = 0; i < p.n; i++) {
      Word w = aSet.get(i);
      if (w.selected) {
        aCount++;
        expA += w.exp;
        respA += w.resp;
        respAs2 += w.resp*w.resp;
        for(int j = 0; j < p.d; j++) {
          controlsA[j] += w.controls[j];
        }
      }
      w = bSet.get(i);
      if (w.selected) {
        bCount++;
        expB += w.exp;
        respB += w.resp;
        respBs2 += w.resp*w.resp;
        for(int j = 0; j < p.d; j++) {
          controlsB[j] += w.controls[j];
        }
      }
    }
    expA = expA/p.k;
    expB = expB/p.k;
    respA = respA/p.k;
    respB = respB/p.k;
    respAs2 = (respAs2/p.k - respA*respA);///(p.k-1);
    respBs2 = (respBs2/p.k - respB*respB);///(p.k-1);
    expDelta = expA-expB;
    respDelta = respA-respB;
    for(int j = 0; j < p.d; j++) {
      controlsA[j] = controlsA[j]/p.k;
      controlsB[j] = controlsB[j]/p.k;
      controlsDelta[j] = controlsA[j]-controlsB[j];
    }
    System.out.printf("|A|=%3d,", aCount);
    System.out.printf("|B|=%3d,", bCount);
    System.out.printf("expl_delta=%.2f", expDelta);
    System.out.print("  ");
    System.out.printf("resp_delta=%.2f,",respDelta);
    System.out.printf("sigma^2=%.2f, ",(respAs2+respBs2));
    System.out.print("  ");
    System.out.print("control deltas=[ ");
    for(int j = 0; j < p.d; j++) {
        System.out.printf("%.2f ",controlsDelta[j]);
    }
    System.out.print("]");
    System.out.print("  ");
    System.out.print("control A=[ ");
    for(int j = 0; j < p.d; j++) {
        System.out.printf("%.2f ",controlsA[j]);
    }
    System.out.print("]           \r");
  }

  public static void main(String[] args) throws IOException {
    double startTime = System.nanoTime();
    Params p = new Params();
    if(loadParams(args,p)) {
      ArrayList<Word> aSet = new ArrayList<Word>();
      ArrayList<Word> bSet = new ArrayList<Word>();
      loadWordSet(p.aName, aSet);
      loadWordSet(p.bName, bSet);
      p.n = aSet.size();
      p.d = aSet.get(0).controls.length;
      if(verifyWordSets(aSet, bSet, p)) {
        p.outFile = new FileWriter(p.outName);
        writeHeader(p.outFile, aSet, bSet, p, RECORD_WEIGHTS);
        System.out.printf("Launching Gurobi.  ");
        GRBObj grbo = new GRBObj();
        initIP(grbo, aSet, bSet, p);
        for( int i=0; i < p.trials; i++ ) {
          getRandomSol(grbo, aSet, bSet, p);
          writeSol(p.outFile, aSet, bSet, p, RECORD_WEIGHTS);
          if (PRINT_TRIAL_SUMMARY) {
            System.out.printf("Trial %4d:  ",i);
            printSummary(aSet, bSet, p);
            if (i % 1000 == 999) {
                System.out.println("");
            }
          }
          if (PRINT_TRIAL_PROGRESS && p.trials > 10
              && i%(p.trials/10)==0)
             System.out.print("\tFinished trial " + i
                                + " ("+i*100/p.trials+"%)\r");
        }
        p.outFile.close();
        System.out.println("\nWrote trial output file: "+p.outName);
        if(GENERATE_XTREME) {
          p.outNameX = p.outName.substring(0,p.outName.length()-4)+"_x.csv";
          p.outFileX = new FileWriter(p.outNameX);
          writeHeader(p.outFileX, aSet, bSet, p, false);
          // Get sol with max response delta
          getXtremeSol(grbo, aSet, bSet, p, true);
          writeSol(p.outFileX, aSet, bSet, p, false);
          if (PRINT_TRIAL_SUMMARY) {
            System.out.print("Max resp delta:");
            printSummary(aSet, bSet, p);
            System.out.println();
          }
          getXtremeSol(grbo, aSet, bSet, p, false);
          // Get sol with min response delta
          writeSol(p.outFileX, aSet, bSet, p, false);
          if (PRINT_TRIAL_SUMMARY) {
            System.out.print("Min resp delta:");
            printSummary(aSet, bSet, p);
            System.out.println();
          }
          /*
          double[] alist = {1, 0.3, 0.1, 0.03, 0.01, 0.003, 0.001, 0.0003, 0.0001};
          for(double alpha : alist) {
            getGoodXtremeSol(grbo, aSet, bSet, p, false, alpha);
            //writeSol(p.outFileX, aSet, bSet, p, false);     // Get sol with min response delta
            if (PRINT_TRIAL_SUMMARY) {
              System.out.println("Low var, low response delta, alpha = "+alpha);
              printSummary(aSet, bSet, p);
            }
          }*/
          p.outFileX.close();
          System.out.println("Wrote extreme output file: "+p.outNameX);
        }
      }
    }
    System.out.println("Total time: "+(System.nanoTime()-startTime)/1000000);
    System.out.println();
  }
}

class GRBObj {
  GRBEnv env;
  GRBModel model;
  GRBVar[] aVars;
  GRBVar[] bVars;
}

class Params {
  int n;
  int d;
  int k;
  int trials;
  double delta;
  String aName;
  String bName;
  String outName;
  FileWriter outFile;
  String outNameX;
  FileWriter outFileX;
}

class Word {
  String word;
  double exp;
  double resp;
  double[] controls;
  double weight;
  boolean selected;

  Word(String word, double exp, double resp, double[] controls) {
    this.word = word;
    this.exp = exp;
    this.resp = resp;
    this.controls = controls;
    this.weight = 0.0;
    this.selected = false;
  }
  public String toString() {
    return word+"\t"+exp+"\t"+resp;
  }
}

class WordWt implements Comparable<WordWt> {
  double weight;
  boolean isSelected;
  WordWt( double weight ) {
    this.weight = weight;
  }
  void setSelected( boolean isSelected ) {
    this.isSelected = isSelected;
  }
  boolean isSelected() {
    return isSelected;
  }
  void reset( double weight, boolean isSelected ) {
    this.weight = weight;
    this.isSelected = isSelected;
  }
  public int compareTo( WordWt w ) {
    return ( this.weight < w.weight ? -1 : 1);
  }
  public String toString() {
    return ( isSelected ? "1" : "0");
  }
}
