divider=echo $(1)'@' | tr ' ' '@' | xargs printf "-----@%-74s\n" | tr '@ ' ' -'

LEX=data/SWR1081.csv
EXT_LEX=data/SWR1081.extended.csv
RAW=data/SWR1081_raw_data.csv
EXT_RAW=data/SWR1081_raw_data.extended.csv
RHO=0.5
DELTA=0.05
K=50
TRIALS=5000
RESP_TEXT=accuracy
RESP=GENEROUS_ACC
RAW_RESP=GENEROUS_ACC

test: test_intro test_python test_gurobi test_R test_latex test_conclusion

full: lexicon
	$(MAKE) frequency
	$(MAKE) degree
	$(MAKE) clustering
	$(MAKE) frequency_full
	$(MAKE) degree_full
	$(MAKE) cv09 
	$(MAKE) agp10
	$(MAKE) cc_k_vary
	$(MAKE) cc_rho_vary
	$(MAKE) synthetic

test_intro: 
	@echo "Running some simple tests to ensure everything needed is here."
	@$(call divider, 'STARTING TESTS')
	@echo

test_conclusion:
	@$(call divider, 'TESTS COMPLETE; EVERYTHING LOOKS GOOD!')

test_python:
	@python -u -c "import sys;\
	  import collections, argparse, math, os.path, random, sys, time;\
	  print '----- Python version ------------------------------------\n',;\
	  print sys.version;\
	  (sys.version_info[0] == 2) and (sys.version_info[1] >= 7) or exit(1);\
	  print 'Python version appears okay.\n'"
	@python -u -c "import numpy, scipy, scipy.stats, pandas, statsmodels;\
	  print '----- Numpy, Scipy, Pandas, Statsmodels version ---------\n',;\
	  print 'Numpy version', numpy.version.version;\
	  print 'Scipy version', scipy.version.version;\
	  print 'Pandas version', pandas.__version__;\
	  print 'Statsmodels version', statsmodels.version.version;\
	  (numpy.version.version > '1.11') or exit(1);\
	  (scipy.version.version > '1.0') or exit(1);\
	  (pandas.__version__ > '0.21') or exit(1);\
	  (statsmodels.version.version > '0.8') or exit(1);\
	  print 'Numpy, scipy, pandas, and statsmodels versions appear okay.\n'"

test_gurobi:
	@echo "----- Java and Gurobi -----------------------------------"
	@echo "GUROBI_HOME: " $$GUROBI_HOME
	@echo "CLASSPATH [gurobi]: " `echo $$CLASSPATH|tr ';' '\n'|grep gurobi`
	@echo "PATH [gurobi]: " `echo $$PATH | tr ':' '\n' | grep gurobi | uniq`
	@if [ "$$GUROBI_HOME" = "" ]; then exit "GUROBI_HOME not set"; fi
	@echo "import gurobi.*; class GurobiTest { \
		  public static void main(String[] args) throws Exception { \
		        GRBObj grbo = new GRBObj(); \
			grbo.env = new GRBEnv();}}" > GurobiTest.java
	@javac GurobiTest.java; java GurobiTest
	@echo "Gurobi, javac, and java appear okay."
	@echo
	@rm GurobiTest.java GurobiTest.class

test_R:
	@echo "----- R (with lme4 package) -----------------------------"
	@Rscript -e "require('lme4')"
	@echo "Rscript (with lme4 package) appears okay."
	@echo 

test_latex:
	@echo "----- pdflatex (with tikz and pgfplots packages) --------"
	@echo "\documentclass{article} \
	  \usepackage{fullpage,tikz,pgfplots,xcolor} \
	  \pgfplotsset{compat=1.10} \
	  \usepgfplotslibrary{fillbetween} \
	  \usetikzlibrary{positioning,fit,matrix,shapes,shapes.callouts} \
	  \usetikzlibrary{decorations.text,external} \
	  \begin{document} \end{document}" | pdflatex -interaction=batchmode
	@echo "pdflatex version and packages appear okay."
	@echo
	@rm article.aux article.log article.pdf



lexicon:
	@$(call divider, 'Augmenting original SWR data with additional fields')
	python compute_lexical_properties.py $(LEX) $(EXT_LEX)
	python extend_raw_swr_data.py $(RAW) $(EXT_RAW)
	@echo

splits:
	@$(call divider, 'Creating directory & splitting on explanatory')
	mkdir -p $(OUTDIR)
	python splits.py $(EXT_LEX) $(RHO) $(OUTDIR) $(RESP) $(EXPL) $(CONTROLS)
	@echo

ipp:	ipp.class
	@$(call divider, 'Running ILP to find $(TRIALS) balanced pairs of sets')
	java ipp $(OUTDIR)/Lz.csv $(OUTDIR)/Hz.csv $(OUTDIR)/ippOut.csv $(K) $(DELTA) $(TRIALS)
	python summarize_ipp.py -l $(EXT_LEX) -d $(OUTDIR) -x $(EXPL) -r $(RESP) -c $(CONTROLS)
	@echo

power_test:
	@$(call divider, 'Running stats to test each balanced pair of sets')
	python power-testing.py $(OUTDIR) -x $(EXPL) -r $(RESP) -d $(OUTDIR)/results_p-values-by-trial.txt -c $(CONTROLS) > $(OUTDIR)/results_power-summary.txt
	Rscript ME_test_trials.R $(OUTDIR)/ippOut.csv $(EXT_RAW) $(OUTDIR) $(RAW_RESP) $(EXPL) $(CONTROLS)

latex:
	@$(call divider, 'Generating and compiling LaTeX files for figures')
	cat extreme_scatter.tex | sed 's/EXPLANATORY/$(EXPL_TEXT)/g; s/RESPONSE/$(RESP_TEXT)/g;' > $(OUTDIR)/extreme_scatter.tex
	cat ipp_scatter.tex | sed 's/EXPLANATORY/$(EXPL_TEXT)/g; s/PARAMETERS/$(PARAMETERS)/g;' > $(OUTDIR)/ipp_scatter.tex
	(cd $(OUTDIR); pdflatex -interaction=batchmode extreme_scatter.tex; echo)
	(cd $(OUTDIR); pdflatex -interaction=batchmode --enable-write18 --extra-mem-bot=10000000 --synctex=1 ipp_scatter.tex; echo)
	(cd $(OUTDIR); enscript -p significance.ps --media Letter results_power-summary.txt; ps2pdf significance.ps; echo)
	@echo



pipeline: splits ipp power_test latex

frequency: OUTDIR=data/output-SWR1081-0.500.frequency.no_controls
frequency: EXPL_TEXT=word frequency
frequency: EXPL=Freq
frequency: PARAMETERS=mark=star,purple
frequency: pipeline

degree: OUTDIR=data/output-SWR1081-0.500.degree.controls_Freq
degree: EXPL_TEXT=number of competitors
degree: EXPL=ELPdegree
degree: CONTROLS=Freq
degree: PARAMETERS=mark=star,orange
degree: pipeline

clustering: OUTDIR=data/output-SWR1081-0.500.CC.controls_Freq+Degree
clustering: EXPL_TEXT=clustering coefficient
clustering: EXPL=ELPCC
clustering: CONTROLS=Freq ELPdegree
clustering: PARAMETERS=mark=star,green
clustering: pipeline

frequency_full: OUTDIR=data/output-SWR1081-0.500.frequency.controls_Degree+CC
frequency_full: EXPL_TEXT=word frequency
frequency_full: EXPL=Freq
frequency_full: CONTROLS=ELPdegree ELPCC
frequency_full: PARAMETERS=mark=star,purple
frequency_full: pipeline

degree_full: OUTDIR=data/output-SWR1081-0.500.degree.controls_Freq+CC
degree_full: EXPL_TEXT=number of competitors
degree_full: EXPL=ELPdegree
degree_full: CONTROLS=Freq ELPCC
degree_full: PARAMETERS=mark=star,orange
degree_full: pipeline

# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2791911/
# In addition to being selected such that the two groups of words differed significantly in clustering coefficient, the two groups of words were selected such that they were equivalent in subjective familiarity, word frequency, neighborhood density, spread of the neighborhood, number of neighbors formed in a given phoneme position, neighborhood frequency, and phonotactic probability.
# Word frequency:  Word frequency refers to how often a word in the language is used. Average log word frequency (log-base 10 of the raw values from Kučera & Francis, 1967) was [...].
cv09: OUTDIR=data/output-Chan_Vitevitch_2009
cv09: EXPL_TEXT=clustering coefficient
cv09: EXPL=ELPCC
cv09: CONTROLS=familiarity Freq ELPdegree ELPspread ELPC1_nbrs ELPV_nbrs ELPC2_nbrs ELPavg_nbr_log_freq phonprob
cv09: PARAMETERS=mark=star,orange
cv09: pipeline

# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3060033/
# We created a set of stimuli that controlled for neighborhood density, word frequency, neighborhood frequency (i.e., frequency weighted neighborhood density; Luce & Pisoni, 1998), phonotactic probability, word length, word familiarity, and syllable structure (CVC) while at the same time manipulating CC.
agp10: OUTDIR=data/output-Altieri_Gruenenfelder_Pisoni_2010
agp10: EXPL_TEXT=clustering coefficient
agp10: EXPL=ELPCC
agp10: CONTROLS=ELPdegree Freq ELPavg_nbr_raw_freq phonprob OrthLength familiarity
agp10: PARAMETERS=mark=star,orange
agp10: pipeline


# -er -.18 -ec .18 -rc .30 comes from COVAR(zscore(x),zscore(y)) x,y in {Generous_ACC, Freq, ELPDegree}.
synthetic: syn_A syn_B syn_C syn_D syn_E syn_F syn_G syn_H syn_I syn_J
syn_%: prefix=`echo $@ | sed 's/syn_//g;'`
syn_%: OUTDIR=data/synthetic/$(prefix)
syn_%: ipp.class
	mkdir -p data/synthetic
	mkdir -p $(OUTDIR)
	python create_simulated_dictionary.py -o $(OUTDIR)/lexicon.csv -er -.18 -ec .18 -rc .30 -n 1081
	python splits.py $(OUTDIR)/lexicon.csv $(RHO) $(OUTDIR) response explanatory control
	java ipp $(OUTDIR)/Lz.csv $(OUTDIR)/Hz.csv $(OUTDIR)/ippOut.csv $(K) $(DELTA) $(TRIALS)
	python power-testing.py $(OUTDIR) -x explanatory -r response -d $(OUTDIR)/results_p-values-by-trial.txt -c control > $(OUTDIR)/results_power-summary.txt
	(cd $(OUTDIR); enscript -p significance.ps --media Letter results_power-summary.txt; ps2pdf significance.ps; echo)

cc_k_vary_%: EXPL_TEXT=clustering coefficient
cc_k_vary_%: EXPL=ELPCC
cc_k_vary_%: CONTROLS=Freq ELPdegree
cc_k_vary_%: PARAMETERS=mark=star,green
cc_k_vary_%: OUTDIR=data/output-SWR1081-k=$(K)-0.500.CC.controls_Freq+Degree

cc_k_vary_25:  K=25
cc_k_vary_25:  pipeline
cc_k_vary_100: K=100
cc_k_vary_100: pipeline
cc_k_vary_200: K=200
cc_k_vary_200: pipeline

cc_k_vary: 
	$(MAKE) cc_k_vary_25
	$(MAKE) cc_k_vary_100
	$(MAKE) cc_k_vary_200

cc_rho_vary_%: EXPL_TEXT=clustering coefficient
cc_rho_vary_%: EXPL=ELPCC
cc_rho_vary_%: CONTROLS=Freq ELPdegree
cc_rho_vary_%: PARAMETERS=mark=star,green
cc_rho_vary_%: OUTDIR=data/output-SWR1081-k=$(K)-0.500.CC.controls_Freq+Degree
cc_rho_vary_%: K=50

cc_rho_vary_0.3: RHO=0.3
cc_rho_vary_0.3: pipeline
cc_rho_vary_0.4: RHO=0.4
cc_rho_vary_0.4: pipeline

cc_rho_vary: 
	$(MAKE) cc_vary_rho_0.3
	$(MAKE) cc_vary_rho_0.4


ipp.class: ipp.java
	javac ipp.java 

figures_and_text:
	@cp data/output-SWR1081-0.500.frequency.no_controls/extreme_scatter.pdf figure1a.pdf
	@cp data/output-SWR1081-0.500.degree.controls_Freq/extreme_scatter.pdf figure1b.pdf
	@cp data/output-SWR1081-0.500.CC.controls_Freq+Degree/extreme_scatter.pdf figure1c.pdf
	@cp data/output-SWR1081-0.500.frequency.no_controls/ipp_scatter.pdf figure3a.pdf
	@cp data/output-SWR1081-0.500.degree.controls_Freq/ipp_scatter.pdf figure3b.pdf
	@cp data/output-SWR1081-0.500.CC.controls_Freq+Degree/ipp_scatter.pdf figure3c.pdf
	@cp data/output-SWR1081-0.500.frequency.controls_Degree+CC/ipp_scatter.pdf figure5a.pdf
	@cp data/output-SWR1081-0.500.degree.controls_Freq+CC/ipp_scatter.pdf figure5b.pdf

	@echo "Testing the effects of explanatory lexical variables using many different pairs of balanced subsets"
	@echo -n " Frequency    (neg|pos|none): "; grep ILP_TT data/output-SWR1081-0.500.frequency.no_controls/results_power-summary.txt
	@echo -n " Degree[Freq] (neg|pos|none): "; grep ILP_TT data/output-SWR1081-0.500.degree.controls_Freq/results_power-summary.txt 
	@echo -n " CC[Deg,Freq] (neg|pos|none): "; grep ILP_TT data/output-SWR1081-0.500.CC.controls_Freq+Degree/results_power-summary.txt
	@echo

	@echo "Figure 4"
	@echo -n " Frequency:        "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.frequency.no_controls/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " Freq[Deg,CC]:     "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.frequency.controls_Degree+CC/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " Degree[Freq]:     "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.degree.controls_Freq/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " Degree[Freq,CC]:  "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.degree.controls_Freq+CC/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " CC[Freq,Deg]:     "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.CC.controls_Freq+Degree/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " CC[AGP10]:        "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-Altieri_Gruenenfelder_Pisoni_2010/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo
	@echo -n " CC[CV09]:         "; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-Chan_Vitevitch_2009/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo

	@echo
	@echo -n " Frequency:        "; egrep -i 'p-value' data/output-SWR1081-0.500.frequency.no_controls/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " Freq[Deg,CC]:     "; egrep -i 'p-value' data/output-SWR1081-0.500.frequency.controls_Degree+CC/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " Degree[Freq]:     "; egrep -i 'p-value' data/output-SWR1081-0.500.degree.controls_Freq/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " Degree[Freq,CC]:  "; egrep -i 'p-value' data/output-SWR1081-0.500.degree.controls_Freq+CC/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " CC[Freq,Deg]:     "; egrep -i 'p-value' data/output-SWR1081-0.500.CC.controls_Freq+Degree/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " CC[AGP10]:        "; egrep -i 'p-value' data/output-Altieri_Gruenenfelder_Pisoni_2010/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo
	@echo -n " CC[CV09]:         "; egrep -i 'p-value' data/output-Chan_Vitevitch_2009/results_power-summary.txt | awk '{printf("%s  |", $$0)}' | xargs echo

	@echo
	@echo "Synthetic (Figure 5):"
	@grep -A 3 covariance data/output-SWR1081-0.500.degree.controls_Freq/results_power-summary.txt | sed 's/ *\]/]/g;' | tr '\n' ' ' | awk '{print $$8, $$9, $$12}' | tr ']' ' ' | xargs echo -n ; echo -n "   #" "SWR  "| tr '#' '\t'; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/output-SWR1081-0.500.degree.controls_Freq/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo

	@for i in A B C D E F G H I J; do (grep -A 3 covariance data/synthetic/$$i/results_power-summary.txt | sed 's/ *\]/]/g;' | tr '\n' ' ' | awk '{print $$8, $$9, $$12}' | tr ']' ' ' | xargs echo -n ; echo -n "   #" $$i "   "| tr '#' '\t'; egrep -i '^[A-Z][A-Z][A-Z]_.*\(' data/synthetic/$$i/results_power-summary.txt | awk '{printf("%s %s  | ", $$1, $$NF)}' | xargs echo ); done | sort -n


clean:
	rm -f *.class *.pyc

fullclean:
	rm -f data/ELP_CVC_graph.csv data/SWR1081.extended.csv
	rm -f data/SWR1081_raw_data.extended.csv 
	rm -r -f data/output-*/ 

