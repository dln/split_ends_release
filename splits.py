'''
splits.py 

usage:
python splits.py input.csv rho outputdir response explanatory controls

Splits a given datafile (represented as a CSV) into two subsets, a
High and a Low subset, defined as the highest rho-fraction of the
data (with respect to the given explanatory variable) and the lowest
rho-fraction.  Also computes z-scores of all numerical fields.
Creates, inside the given outputdir, four files:
  -- H.csv and L.csv (high/low CSV with original numerical scores).
  -- Hz.csv and Lz.csv (high/low CSV with z-scores).
Includes only those columns of the given CSV that are one of response, 
explanatory, or one of the control variables.

This version:  December 2017.
'''

import pandas
import time
import argparse

parser = argparse.ArgumentParser(description='Split CSV into high/low')
parser.add_argument('datafile', type=str, help="filename of the CSV to split")
parser.add_argument('rho', type=float, help="fraction that counts as extreme")
parser.add_argument('outputdir', type=str, help="directory for output")
parser.add_argument('response', type=str, help="response variable")
parser.add_argument('explanatory', type=str, help="explanatory variable")
parser.add_argument(dest='controls', nargs = "*", help='control variables')
args = parser.parse_args()

def dump_file(filename, df, columns, reference_df_size, zscore=False):
    '''
    Writes all identified columns from the dataframe df into filename
    as a CSV, with appropriate header information including the size 
    of the resulting CSV (which is used by the Java ILP).
    '''
    num_rows = len(df.index)
    num_controls = len(columns) - 3  # excluding: word, response, explanatory

    f = open(filename, "w")
    f.write("# pandas dump of WordSet generated at " + time.ctime() + "\n")
    f.write("# after these comments, there are two special lines:\n")
    f.write("#   -- a list of fields (word, response, explanatory, controls)\n")
    f.write("#   -- the instance size (n,d)\n")
    f.write("# where n==number of words, d==number of control variables.\n")
    if zscore:
        f.write("# all numerical fields are zScores with respect to " +
                "a big ole WordSet with %d words.\n" % reference_df_size)
    column_descriptions = ",".join(columns) + "\n"
    instance_size_description = "%d,%d\n" % (num_rows, num_controls)
    f.write(column_descriptions)
    f.write(instance_size_description)
    df.to_csv(f, columns=columns, header=False, index=False)
    f.close()

raw_variables = [args.response, args.explanatory] + args.controls
z_variables = [variable + "-zscore" for variable in raw_variables]

# Note:  the na_values and keep_default_na flags in the following lines are
# here because 'null' is one of the words in the data, and pandas was doing 
# nasty things and translating 'null' into NaN and then crashing.
# See <https://github.com/pandas-dev/pandas/issues/1657>.
words_df = pandas.read_csv(args.datafile, na_values=[''], keep_default_na=False)
words_df.sort_values(args.explanatory, inplace=True)
for col in raw_variables:
    col_zscore = col + '-zscore'
    mean = words_df[col].mean()
    std = words_df[col].std(ddof=0)
    words_df[col_zscore] = (words_df[col] - mean) / std

n = len(words_df.index)
L = words_df.head(int(n * args.rho))
H = words_df.tail(int(n * args.rho))

dump_file(args.outputdir+"/H.csv",  H, ["word"] + raw_variables, n)
dump_file(args.outputdir+"/L.csv",  L, ["word"] + raw_variables, n)
dump_file(args.outputdir+"/Hz.csv", H, ["word"] + z_variables, n, zscore=True)
dump_file(args.outputdir+"/Lz.csv", L, ["word"] + z_variables, n, zscore=True)
