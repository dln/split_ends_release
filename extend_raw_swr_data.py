'''
extend_raw_data.py

usage:
python extend_raw_data.py input.csv output.csv

Assumes input.csv has the target word as its first entry.
Augments the first (header) line with new column names and 
adds a variety of ELP neighborhood-related statistics to 
each other (word) line.

This version:  May 2018.
'''

from collections import defaultdict
import os.path
import sys
import pandas
import ldr_words
import argparse
from compute_lexical_properties import Field as Field

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="add ELP stats to lexicon")
    parser.add_argument('inputCSV', type=str, help = "raw lexicon CSV")
    parser.add_argument('outputCSV', type=str, help = "output lexicon CSV")
    args = parser.parse_args()

    if os.path.isfile(args.outputCSV):
        sys.stderr.write("Found extended raw data file %s.\n" % args.outputCSV)
        sys.stderr.write("I assume it's accurate, and I'm not rebuilding it.\n")
        sys.stderr.write("You can 'make fullclean' if you need to rebuild.\n")
        exit(0)
    
    # Note:  the na_values and keep_default_na flags in the following lines are
    # here because 'null' is one of the words in the data, and pandas was doing 
    # nasty things and translating 'null' into NaN and then crashing.
    # See <https://github.com/pandas-dev/pandas/issues/1657>.
    raw_results = pandas.read_csv(args.inputCSV,
                                  na_values=[''], keep_default_na=False)
    ELP_data = pandas.read_csv("data/SWR1081.extended.csv",
                               na_values=[''], keep_default_na=False)

    f_out = open(args.outputCSV, "w")
    ELP_fields = ",".join(list(ELP_data)).replace("word,", "CRESP_HOMOPHONE,")
    f_out.write("%s,%s\n" % (",".join(list(raw_results)), ELP_fields))
    for i in range(len(raw_results)):
        entry = raw_results.iloc[i]
        my_word = entry.CRESP
        if ELP_data.loc[ELP_data['word'] == my_word].empty:
            my_word = ELP_data.loc[ELP_data['Original_Orthography'] == my_word].word.values[0]

        values = list(ELP_data.loc[ELP_data['word'] == my_word].values[0])
        fieldstr = ",".join([str(x) for x in list(entry.values) + list(values)])
        f_out.write(fieldstr + "\n")
    f_out.close()
    sys.stderr.write("Added ELP statistics into %s.\n" % args.outputCSV)
