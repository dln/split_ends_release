'''
compute_lexical_properties.py 

usage:
python compute_lexical_properties.py input.csv output.csv

Assumes input.csv has the target word as its first entry.
Augments the first (header) line with new column names and 
adds a variety of ELP neighborhood-related statistics to 
each other (word) line.

This version:  May 2018.
'''

from collections import defaultdict
import os.path
import sys
import ldr_words
import argparse

def build_CVC_graph(lexicon, output_file, radius=1):
    '''
    lexicon == a Lexicon of words.  (see ldr_words)
    radius == an integer.
    
    All words w with edit_distance(w,seed) <= radius are included,
    where seed is any CVC word.  Dumps to the given output_file, with lines 
       word,nbr1,nbr2,...,nbrN.
    '''
    seeds = [w for w in lexicon.get_words() if w.get_cv_pattern() == "CVC"]
    neighbors = defaultdict(list)
            
    for word in lexicon.get_words():
        if not (3 - radius <= len(word.get_cv_pattern()) <= 3 + radius):
            # There's no way that this word is within one edit of any CVC!
            continue
        for seed in seeds:
            if seed.edit_distance(word) == 1:
                if word not in neighbors[seed]:
                    neighbors[seed].append(word)
                    neighbors[word].append(seed)
    f = open(output_file, "w")
    for word in neighbors:
        f.write(",".join(map(lambda w:w.get_orthography(), 
                             [word] + neighbors[word])))
        f.write("\n")
    f.close()


def load_CVC_graph(graph_file):
    '''
    Load an adjacency list dictionary (neighbors[word] gives a list of word's
    neighbors) from a precomputed CVC graph from a file.  (See build_CVC_graph.)
    '''
    neighbors = {}
    f = open(graph_file)
    for line in f:
        fields = line.strip().split(",")
        neighbors[fields[0]] = fields[1:]
    f.close()
    return neighbors

def degree(ego, adjlist):
    '''
    The number of neighbors ego has in the adjlist dictionary (-1 if not found).
    '''
    if ego not in adjlist:
        return -1
    return len(adjlist[ego])

def avg_nbr_log_freq(ego, adjlist, lexicon):
    '''
    The average log frequency of all of ego's neighbors (-1 if not found).
    '''
    if ego not in adjlist:
        return -1
    neighbor_frequencies = [lexicon.get_word(nbr).get_log_frequency()
                            for nbr in adjlist[ego]]
    # Replace "None"s by 0s for frequencies.
    neighbor_frequencies = [x if x else 0 for x in neighbor_frequencies]
    return sum(neighbor_frequencies) * 1. / len(neighbor_frequencies)

def avg_nbr_raw_freq(ego, adjlist, lexicon):
    '''
    The average (raw) frequency of all of ego's neighbors (-1 if not found).
    '''
    if ego not in adjlist:
        return -1
    neighbor_frequencies = [lexicon.get_word(nbr).get_raw_frequency()
                            for nbr in adjlist[ego]]
    # Replace "None"s by 0s for frequencies.
    neighbor_frequencies = [x if x else 0 for x in neighbor_frequencies]
    return sum(neighbor_frequencies) * 1. / len(neighbor_frequencies)

def clustering_coefficient(ego, adjlist, p=1):
    '''
    The clustering coefficient of ego in the adjlist dictionary (-1 if ego 
    is not present).
    '''
    if ego not in adjlist:
        return -1
    neighbors = adjlist[ego]
    num_nbr_pairs = len(neighbors) * (len(neighbors) - 1.0)
    nbr_edges = [(x,y) for x in adjlist[ego] for y in adjlist[ego] 
                 if y in adjlist[x]]
    return len(nbr_edges) / num_nbr_pairs

def positional_spread(ego, adjlist, pos, lexicon):
    '''
    The phonological spread of ego in the adjlist dictionary (-1 if ego 
    is not present) -- i.e., the number of positions I in ego's pronunciation
    in which a neighbor resulting from a substitution for phoneme I exists.
    '''
    ego_pron = lexicon.get_word(ego).get_pronunciation()
    count = 0
    for neighbor in adjlist[ego]:
        nbr_pron = lexicon.get_word(neighbor).get_pronunciation()
        if len(nbr_pron) == len(ego_pron) and ego_pron[pos] != nbr_pron[pos]:
            count += 1
    return count

def phonological_spread(ego, adjlist, lexicon):
    '''
    The phonological spread of ego in the adjlist dictionary (-1 if ego 
    is not present) -- i.e., the number of positions I in ego's pronunciation
    in which a neighbor resulting from a substitution for phoneme I exists.
    '''
    onset_count = positional_spread(ego, adjlist, 0, lexicon)
    vowel_count = positional_spread(ego, adjlist, 1, lexicon)
    coda_count  = positional_spread(ego, adjlist, 2, lexicon)
    return (onset_count > 0) + (vowel_count > 0) + (coda_count > 0)

def load_ELP_CVC_graph_file(ELP_CVC_graph_file, ELP_lexicon):
    '''
    Load the ELP CVC graph from the given file (if it already exists),
    (re)building it from scratch if the file doesn't exist.
    '''
    if not os.path.isfile(ELP_CVC_graph_file):
        sys.stderr.write("No precomputed CVC graph file found; building it.\n")
        sys.stderr.write("This may take some time.\n")
        build_CVC_graph(ELP_lexicon, ELP_CVC_graph_file, radius=1)
    else:
        sys.stderr.write("Found precomputed CVC graph file %s.\n" %
                         ELP_CVC_graph_file)
        sys.stderr.write("I'm assuming it's accurate; I'm not rebuilding it.\n")
    return load_CVC_graph(ELP_CVC_graph_file)


class Field:
    '''
    Simple class pairing a variable name and a function f(word) mapping a given
    word to its corresponding variable value.
    '''
    def __init__(self, field_name, field_function):
        self.field_name = field_name
        self.field_function = field_function
    def get_name(self): return self.field_name
    def get_value(self, word): return self.field_function(word)

    
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="add ELP stats to lexicon")
    parser.add_argument('inputCSV', type=str, help = "raw lexicon CSV")
    parser.add_argument('outputCSV', type=str, help = "output lexicon CSV")
    args = parser.parse_args()

    lexicon = ldr_words.Lexicon("data/ELP_40k_lexicon.csv")
    adjlist = load_ELP_CVC_graph_file("data/ELP_CVC_graph.csv", lexicon)

    # The data fields that will be added to the input CSV.
    extension_fields = [
     Field("ELPdegree",  lambda w: degree(w,adjlist)),
     Field("ELPCC",      lambda w: clustering_coefficient(w,adjlist,p=1)),
     Field("ELPC1_nbrs", lambda w: positional_spread(w,adjlist,0,lexicon)),
     Field("ELPV_nbrs",  lambda w: positional_spread(w,adjlist,1,lexicon)),
     Field("ELPC2_nbrs", lambda w: positional_spread(w,adjlist,2,lexicon)),
     Field("ELPspread",  lambda w: phonological_spread(w,adjlist,lexicon)),
     Field("ELPavg_nbr_log_freq",lambda w: avg_nbr_log_freq(w,adjlist,lexicon)),
     Field("ELPavg_nbr_raw_freq",lambda w: avg_nbr_raw_freq(w,adjlist,lexicon))
    ]
    
    f_in = open(args.inputCSV)
    f_out = open(args.outputCSV, "w")
    missing_ELP = 0
    word_count = 0
    for line in f_in:
        if word_count == 0:
            # Output the extended header line.
            names = [field.get_name() for field in extension_fields]
            f_out.write("%s,%s\n" % (line.strip(), ",".join(names)))
        else:
            # Output the extended data line.
            word_fields = line.strip().split(",")
            word, stats = word_fields[0], word_fields[1:]
            assert lexicon.has_word(word)
            assert lexicon.get_word(word).get_cv_pattern() == "CVC"
            values = [field.get_value(word) for field in extension_fields]
            if -1 in values:
                sys.stderr.write("Warning:  %s not found in ELP. " % word)
                sys.stderr.write("Have you considered a homophone?\n")
                missing_ELP += 1
                values = ["" for v in values] # nothing to write
            values = map(lambda x:str(x), values)
            f_out.write("%s,%s\n" % (line.strip(), ",".join(values)))
        word_count += 1
    f_in.close()
    f_out.close()
    sys.stderr.write("Added ELP statistics into %s.\n" % args.outputCSV)
    if missing_ELP:
        sys.stderr.write("Warning:  there were %d words in %s not " +
                         "found in ELP!\n\n" % (missing_ELP, args.inputCSV))
    sys.stderr.write("\n")
